provider "aws" {
 region = "us-east-2"
}

module "iam-users" {
  source  = "mineiros-io/iam-user/aws"
  version = "~> 0.4.0"

  names = [
    "user.one",
    "user.two",
    "user.three",
  ]

  policy_arns = [
    "arn:aws:iam::aws:policy/ReadOnlyAccess",
    "arn:aws:iam::aws:policy/job-function/Billing",
  ]
}
